# -*- coding: utf-8 -*-

import tkinter as tk
from tkinter import *
from tkinter import ttk
from PIL import ImageTk,Image
from tkcalendar import Calendar,DateEntry
import datetime
from datetime import datetime
from utilite import Utilite
from analitico import Analitic

class Tela:
    
    def updateDados(self,banco):
        tupla = self.listboxBuscar.get(ACTIVE)
        if tupla!="":
            tuplaSplit = tupla.split()
            self.newWindow = tk.Toplevel(self.master,width=500, height=500)
            self.newWindow.title("Editar")
            self.newWindowLinhaUm= Frame(self.newWindow)
            self.newWindowLinhaUm["pady"] = 5
            self.newWindowLinhaUm.pack()
            
            
            self.newWindowtipoNovoLabelInserir = Label(self.newWindowLinhaUm,text="Tipo:",width =10)
            self.newWindowtipoNovoLabelInserir.pack(side=LEFT, anchor =NW)

            self.newWindowtipoNovoTextInserir = Text(self.newWindowLinhaUm,height=1, width=10)
            self.newWindowtipoNovoTextInserir.insert(1.0,str(tuplaSplit[0]))
 
            self.newWindowtipoNovoTextInserir.pack(side=LEFT, anchor =NW)  
            
            #Linha Dois
            self.newWindowLinhaDois = Frame(self.newWindow)
            self.newWindowLinhaDois["pady"] = 5
            self.newWindowLinhaDois.pack()
            
            self.newWindowvalorLabelInserir = Label(self.newWindowLinhaDois,text="Valor:",width =7)
            self.newWindowvalorLabelInserir.pack(side=LEFT, anchor =NW)
            
            self.newWindowvalorTextInserir = Text(self.newWindowLinhaDois,height=1, width=15)
            self.newWindowvalorTextInserir.insert(1.0,str(tuplaSplit[1]))
            self.newWindowvalorTextInserir.pack(side=LEFT, anchor =NW)
            

            self.newWindowdataLabelInserir = Label(self.newWindowLinhaDois,text="Data:",width =7)
            self.newWindowdataLabelInserir.pack(side=LEFT, anchor =NW)
            
            
                

            dataSplit =tuplaSplit[2].split("/")
            
            self.newWindowdataInserir = DateEntry(self.newWindowLinhaDois,width=30,bg="darkblue",fg="white",year=int(dataSplit[2]), month=int(dataSplit[1]), day=int(dataSplit[0]))
            self.newWindowdataInserir.pack(side=LEFT, anchor =NW)
            
            #-----
            #Linha Três
            
            self.newWindowLinhaTr3s = Frame(self.newWindow)
            self.newWindowLinhaTr3s["pady"] = 5
            self.newWindowLinhaTr3s.pack()
            

            dataNova = self.utilite.dateFormChange(tuplaSplit[2], '%d/%m/%Y', '%Y-%m-%d') 
            
            
            descri = banco.selectDescri(tuplaSplit[0], dataNova, tuplaSplit[1])
            
            self.newWindowdescricaoLabelInserir = Label(self.newWindowLinhaTr3s,text="Descrição:",width =10)
            self.newWindowdescricaoLabelInserir.pack(side=LEFT, anchor =NW)
            
            self.newWindowdescricaoTextInserir = Text(self.newWindowLinhaTr3s,height=5, width=30)
            
            self.newWindowdescricaoTextInserir.insert(1.0,descri[0][0])
            self.newWindowdescricaoTextInserir.pack(side=LEFT, anchor =NW)
            
            
            
            
            #-------
            #Linha quatro
            #Botão Update
                    
            self.newWindowLinhaQuatro = Frame(self.newWindow)
            self.newWindowLinhaQuatro["pady"] = 10
            self.newWindowLinhaQuatro.pack()
            #self.bntSalvarInserir = Button(self.AbaUmLinhaQuatro)
            self.bntSalvarUpdate = Button(self.newWindowLinhaQuatro,command=lambda: self.executarUpdate(banco,tuplaSplit[0],dataNova,tuplaSplit[1],descri[0][0]))
            self.bntSalvarUpdate["text"] = "Update"
            self.bntSalvarUpdate["font"] = ("Calibri", "8")
            self.bntSalvarUpdate["width"] = 12
            self.bntSalvarUpdate.pack( )




    def executarUpdate(self,banco,tuplaSplitZero,dataNova,tuplaSplitUm,descri):
       
        dataSet = self.utilite.dateFormChange(self.newWindowdataInserir.get(), '%d/%m/%Y', '%Y-%m-%d')          
        banco.update(tuplaSplitZero,dataNova,tuplaSplitUm,descri,
                     self.newWindowtipoNovoTextInserir.get("1.0", "end-1c"),dataSet,
                     self.newWindowvalorTextInserir.get("1.0", "end-1c"),
                     self.newWindowdescricaoTextInserir.get("1.0", "end-1c"))        
        self.newWindow.destroy()



    def executarBuscar(self,r):
        if self.listboxBuscar.size() >0:
            self.listboxBuscar.delete(0,tk.END)
        for i in range(len(r)):
            
            newformat = self.utilite.dateFormChange( r[i][2], '%Y-%m-%d', '%d/%m/%Y') 
            
            display = r[i][0]+" " +str(r[i][1])+" "+ newformat
            self.listboxBuscar.insert(END, display)

        
        

    
    def getDados(self,banco):
            #tipo 
        tipo = self.tipoTextBuscar.get("1.0", "end-1c")
        self.tipoTextBuscar.delete(1.0,tk.END)
    #    #valor
    #    valor = self.valotTextBucar.get("1.0", "end-1c")
    #    self.valotTextBucar.delete(1.0,tk.END)
        #data


        data = self.utilite.dateFormChange( self.dataTextBuscar.get(), '%d/%m/%Y', '%Y-%m-%d') 
        
        dataFim = self.utilite.dateFormChange( self.dataTextBuscarFim.get(), '%d/%m/%Y', '%Y-%m-%d') 


        descr = self.descricaoTextInserir.get("1.0", "end-1c")
        self.descricaoTextInserir.delete(1.0,tk.END)
        

        self.executarBuscar(banco.Select(tipo,data,dataFim))
            
            
    def salvarDados(self,banco):
            
        if self.listboxTipoInserir.size()==0 or self.tipoNovoTextInserir.get("1.0", "end-1c") !="" :
            tipo = self.tipoNovoTextInserir.get("1.0", "end-1c")
        else:
            tipo = self.listboxTipoInserir.get(ACTIVE)
            
        valor = self.valorTextInserir.get("1.0", "end-1c")
        descricao = self.descricaoTextInserir.get("1.0", "end-1c")
                
        data = self.utilite.dateFormChange( self.dataInserir.get(), '%d/%m/%Y', '%Y-%m-%d') 

            
        
        if tipo !="" and valor !="" and data !="" and descricao !="":
            banco.InserirDAdo(tipo,data,valor,descricao)
            self.tipoNovoTextInserir.delete(1.0,tk.END)
            self.valorTextInserir.delete(1.0,tk.END)
            self.descricaoTextInserir.delete(1.0,tk.END)
            self.listboxTipoInserir.delete(0,tk.END)
            

            listaTipo = banco.selectTipos()
            
            for i in range(len(listaTipo)): 
                self.listboxTipoInserir.insert(END, listaTipo[i][0]) 
        else:
            messagebox.showerror("Erro", "Dados Faltando.")
            
    def deletarDados(self,banco):
        

        tupla = self.listboxBuscar.get(ACTIVE)
        if tupla!="":
            tuplaSplit = tupla.split()
            
            
            dataNova = self.utilite.dateFormChange( tuplaSplit[2], '%d/%m/%Y', '%Y-%m-%d')
            
            banco.Delete(tuplaSplit[0],dataNova,tuplaSplit[1])


            messagebox.showerror("!!!", "Dados Deletados.")
            
    def checkBoxMesAnoActive(self):
        if  self.checkBoxMesAnoVar.get() == 1 :
            self.checkBoxEntreDatasVar.set(0)
            self.AnosListBox.config(state='abled')
            self.MesListBox.config(state='abled')
            self.dataTextAnalise.config(state='disabled')
            self.dataTextAnaliseFim.config(state='disabled')
        if self.checkBoxMesAnoVar.get() == 0: 
            self.AnosListBox.config(state='disabled')
            self.MesListBox.config(state='disabled')
             
             
    def checkBoxEntreDatasActive(self):
        if self.checkBoxEntreDatasVar.get() == 1 :
            self.checkBoxMesAnoVar.set(0)
            self.AnosListBox.config(state='disabled')
            self.MesListBox.config(state='disabled')
            self.dataTextAnalise.config(state='abled')
            self.dataTextAnaliseFim.config(state='abled')
            
        if self.checkBoxEntreDatasVar.get() == 0 :
             self.dataTextAnalise.config(state='disabled')
             self.dataTextAnaliseFim.config(state='disabled')
    
    def analiseDeDados(self,banco):
        if self.checkBoxEntreDatasVar.get() == 1:
            
            dataInicio = self.utilite.dateFormChange( self.dataTextAnalise.get(), '%d/%m/%Y', '%Y-%m-%d') 
        
            dataFim = self.utilite.dateFormChange( self.dataTextAnaliseFim.get(), '%d/%m/%Y', '%Y-%m-%d')
            
            pie,bar,gastoTotal =self.analitico.analiseEntreDatas(banco,dataInicio,dataFim)
            #começar por aqui
            #abrir nova Janela Com os dos gráficos
            
        if self.checkBoxMesAnoVar.get() == 1:
            messagebox.showerror("Erro", "USE A OPÇÃO DE CIMA")
            
            




    def criacaoDaTela(self, banco):
  
        self.abas = ttk.Notebook(self.master,width=700, height=500)
        self.Aba2 = Frame(self.abas)
        self.Aba1 = Frame(self.abas)
        self.Aba3 = Frame(self.abas)
        
        self.Aba1["pady"] = 10
        
      
        #-----------------------------------
          #Aba dois

        
        #Linha Um
        self.AbaDoisLinhaUm = Frame(self.Aba2)
        self.AbaDoisLinhaUm["pady"] = 5
        self.AbaDoisLinhaUm.pack()
        
        self.tipoLabelBuscar = Label(self.AbaDoisLinhaUm,text="Tipo:",width =10)
        self.tipoLabelBuscar.pack(side=LEFT, anchor =NW)
        
        self.tipoTextBuscar = Text(self.AbaDoisLinhaUm,height=1, width=20)
        self.tipoTextBuscar.pack(side=LEFT, anchor =NW)
        
        #-----
                #-----------------------------------
        
        #Linha Dois
        self.AbaDoisLinhaDois = Frame(self.Aba2)
        self.AbaDoisLinhaDois["pady"] = 5
        self.AbaDoisLinhaDois.pack()
        
      #  self.valorLabelBuscar = Label(self.AbaDoisLinhaDois,text="Valor:",width =7)
       # self.valorLabelBuscar.pack(side=LEFT, anchor =NW)
        
       # self.valotTextBucar = Text(self.AbaDoisLinhaDois,height=1, width=5)
       # self.valotTextBucar.pack(side=LEFT, anchor =NW,padx=10)
        
        
        now = datetime.now()   

        
        
        self.dataLabelBuscar = Label(self.AbaDoisLinhaDois,text="Data Inicio:",width =7)
        self.dataLabelBuscar.pack(side=LEFT, anchor =NW,padx=10)
        
        self.dataTextBuscar = DateEntry(self.AbaDoisLinhaDois,width=30,bg="darkblue",fg="white",year=now.year)
        self.dataTextBuscar.pack(side=LEFT, anchor =NW)
        
        self.dataLabelBuscar = Label(self.AbaDoisLinhaDois,text="Data Fim:",width =7)
        self.dataLabelBuscar.pack(side=LEFT, anchor =NW,padx=10)
        
        self.dataTextBuscarFim = DateEntry(self.AbaDoisLinhaDois,width=30,bg="darkblue",fg="white",year=now.year)
        self.dataTextBuscarFim.pack(side=LEFT, anchor =NW)
        
        #-----
        #Linha tres
        
        #self.AbaDoisLinhaTres = Frame(self.Aba2)
        #self.AbaDoisLinhaTres["pady"] = 5
        #self.AbaDoisLinhaTres.pack()
        
       # self.descricaoLabelBuscar = Label(self.AbaDoisLinhaTres,text="Descrição:",width =10)

       # self.descricaoLabelBuscar .pack(side=LEFT, anchor =NW)
        
        #self.descricaoTextBuscar  = Text(self.AbaDoisLinhaTres,height=5, width=30)
        #self.descricaoTextBuscar.pack(side=LEFT, anchor =NW)
        
        
        #-------
        #Linha Qautro
        #Botão Buscar
        
                
        self.AbaDoisLinhaQuatro = Frame(self.Aba2)
        self.AbaDoisLinhaQuatro["pady"] = 10
        self.AbaDoisLinhaQuatro.pack()
        
        
        self.bntBuscar = Button(self.AbaDoisLinhaQuatro,command=lambda: self.getDados(banco))
        self.bntBuscar["text"] = "Buscar"
        self.bntBuscar["font"] = ("Calibri", "8")
        self.bntBuscar["width"] = 12
        self.bntBuscar.pack( )
        
        
        #-----------
        #Linha 5
        

        
        
        self.primeiroContainerX13 = Frame(self.Aba2)
        self.primeiroContainerX13["pady"] = 5
        self.primeiroContainerX13.pack()
        
        
        
        self.scrollbarlistboxBuscar = Scrollbar(self.primeiroContainerX13) 
        self.scrollbarlistboxBuscar.pack(side = RIGHT, fill = Y) 
    
        self.listboxBuscar = Listbox(self.primeiroContainerX13,width =40,height=10) 
        self.listboxBuscar.pack(side = LEFT, fill = BOTH)   
                
        self.listboxBuscar.config(yscrollcommand = self.scrollbarlistboxBuscar.set) 
          
        self.scrollbarlistboxBuscar.config(command = self.listboxBuscar.yview)
            

        self.primeiroContainerX133 = Frame(self.Aba2)
        self.primeiroContainerX133["pady"] = 5
        self.primeiroContainerX133.pack()
                
        self.bntEditar = Button(self.primeiroContainerX133,command=lambda: self.updateDados(banco))
        self.bntEditar["text"] = "Editar"
        self.bntEditar["font"] = ("Calibri", "8")
        self.bntEditar["width"] = 12
        self.bntEditar.pack(side = LEFT)
        
        self.bntDeletar = Button(self.primeiroContainerX133,command=lambda: self.deletarDados(banco))
        self.bntDeletar["text"] = "Deletar"
        self.bntDeletar["font"] = ("Calibri", "8")
        self.bntDeletar["width"] = 12
        self.bntDeletar.pack(side = LEFT)
        

        
          #-----------------------------------------------------
          #-----------------------------------------------------
          #-----------------------------------------------------
          #-----------------------------------------------------
          #-----------------------------------------------------
          #-----------------------------------------------------
          #-----------------------------------------------------
          
        #Aba um        
        #Linha Um
        self.AbaUmLinhaUm= Frame(self.Aba1)
        self.AbaUmLinhaUm["pady"] = 5
        self.AbaUmLinhaUm.pack()
        
        
        self.tipoNovoLabelInserir = Label(self.AbaUmLinhaUm,text="Novo Tipo:",width =10)
        self.tipoNovoLabelInserir.pack(side=LEFT, anchor =NW)
        
        self.tipoNovoTextInserir = Text(self.AbaUmLinhaUm,height=1, width=10)
        self.tipoNovoTextInserir.pack(side=LEFT, anchor =NW)
        
        
        self.tipoLabelInserir = Label(self.AbaUmLinhaUm,text="Tipo:",width =10)
        self.tipoLabelInserir.pack(side=LEFT, anchor =NW)
        
        self.scrollbarlistboxTipoInserir = Scrollbar(self.AbaUmLinhaUm) 
        self.scrollbarlistboxTipoInserir.pack(side = RIGHT, fill = Y) 
    
        self.listboxTipoInserir = Listbox(self.AbaUmLinhaUm,width =20,height=5) 
        self.listboxTipoInserir.pack(side = LEFT, fill = BOTH)   



        listaTipo = banco.selectTipos()
        for i in range(len(listaTipo)): 
            self.listboxTipoInserir.insert(END, listaTipo[i][0]) 
                
        self.listboxTipoInserir.config(yscrollcommand = self.scrollbarlistboxTipoInserir.set) 
          
        self.scrollbarlistboxTipoInserir.config(command = self.listboxTipoInserir.yview)
            

            
                

            
        
        #Linha Dois
        self.AbaUmLinhaDois = Frame(self.Aba1)
        self.AbaUmLinhaDois["pady"] = 5
        self.AbaUmLinhaDois.pack()
        
        self.valorLabelInserir = Label(self.AbaUmLinhaDois,text="Valor:",width =7)
        self.valorLabelInserir.pack(side=LEFT, anchor =NW)
        
        self.valorTextInserir = Text(self.AbaUmLinhaDois,height=1, width=15)
        self.valorTextInserir.pack(side=LEFT, anchor =NW)
        
        
        self.dataLabelInserir = Label(self.AbaUmLinhaDois,text="Data:",width =7)
        self.dataLabelInserir.pack(side=LEFT, anchor =NW)
        
        
        now = datetime.now()   
        self.dataInserir = DateEntry(self.AbaUmLinhaDois,width=30,bg="darkblue",fg="white",year=now.year)
        self.dataInserir.pack(side=LEFT, anchor =NW)
        
        #-----
        #Linha Três
        
        self.AbaUmLinhaTr3s = Frame(self.Aba1)
        self.AbaUmLinhaTr3s["pady"] = 5
        self.AbaUmLinhaTr3s.pack()
        
        self.descricaoLabelInserir = Label(self.AbaUmLinhaTr3s,text="Descrição:",width =10)
        self.descricaoLabelInserir.pack(side=LEFT, anchor =NW)
        
        self.descricaoTextInserir = Text(self.AbaUmLinhaTr3s,height=5, width=30)
        self.descricaoTextInserir.pack(side=LEFT, anchor =NW)
        
        
        #-------
        #Linha quatro
        #Botão Inserir
        
                
        self.AbaUmLinhaQuatro = Frame(self.Aba1)
        self.AbaUmLinhaQuatro["pady"] = 10
        self.AbaUmLinhaQuatro.pack()
        #self.bntSalvarInserir = Button(self.AbaUmLinhaQuatro)
        self.bntSalvarInserir = Button(self.AbaUmLinhaQuatro, command=lambda: self.salvarDados(banco))
        self.bntSalvarInserir["text"] = "Salvar"
        self.bntSalvarInserir["font"] = ("Calibri", "8")
        self.bntSalvarInserir["width"] = 12
        self.bntSalvarInserir.pack( )
        

        
        
        #-----------------------------------------
        #Aba de Analise
        #graficos
            #analise por tipo
                # analise do valor do tipo em relação ao total do mes e do ano
                    #analise do valor do tipo em relação a 3 meses anteriores
            #, por data, por tipo e data
        self.AbaTresLinhaUm = Frame(self.Aba3)
        self.AbaTresLinhaUm["pady"] = 5
        self.AbaTresLinhaUm.pack()
        
        self.checkBoxEntreDatasVar = tk.IntVar()
        
        #, command=print_selection2
        self.checkBoxEntreDatas = tk.Checkbutton(self.AbaTresLinhaUm, text='Entre Datas',
                                                 variable= self.checkBoxEntreDatasVar, onvalue=1,
                                                 offvalue=0, command = self.checkBoxEntreDatasActive)
        self.checkBoxEntreDatas.pack(side=LEFT)
        
        
        self.AbaTresLinhaDois = Frame(self.Aba3)
        self.AbaTresLinhaDois["pady"] = 5
        self.AbaTresLinhaDois.pack()
        
        self.dataLabelAnalise = Label(self.AbaTresLinhaDois,text="Data Inicio:",width =7)
        self.dataLabelAnalise.pack(side=LEFT, anchor =NW,padx=10)
        
        self.dataTextAnalise = DateEntry(self.AbaTresLinhaDois,width=30,bg="darkblue",
                                         fg="white",year=now.year)
        self.dataTextAnalise.config(state='disabled')
        self.dataTextAnalise.pack(side=LEFT, anchor =NW)
        
        self.dataLabelAnalise = Label(self.AbaTresLinhaDois,text="Data Fim:",width =7)
        self.dataLabelAnalise.pack(side=LEFT, anchor =NW,padx=10)
        
        self.dataTextAnaliseFim = DateEntry(self.AbaTresLinhaDois,width=30,bg="darkblue",
                                            fg="white",year=now.year)
        self.dataTextAnaliseFim.config(state='disabled')
        self.dataTextAnaliseFim.pack(side=LEFT, anchor =NW)


        
        self.AbaTresLinhaTres = Frame(self.Aba3)
        self.AbaTresLinhaTres["pady"] = 5
        self.AbaTresLinhaTres.pack()
        
        self.checkBoxMesAnoVar = tk.IntVar()
        
        self.checkBoxMesAno = tk.Checkbutton(self.AbaTresLinhaTres, text='Mês e Ano',
                                             variable= self.checkBoxMesAnoVar, onvalue=1, offvalue=0,
                                             command= self.checkBoxMesAnoActive)
        
        self.checkBoxMesAno.pack(side=LEFT)
        
        self.AbaTresLinhaQuatro = Frame(self.Aba3)
        self.AbaTresLinhaQuatro["pady"] = 5
        self.AbaTresLinhaQuatro.pack()
        
        self.MesLabelAnalise = Label(self.AbaTresLinhaQuatro,text="Mês:",width =7)
        self.MesLabelAnalise.pack(side=LEFT, anchor =NW,padx=1)
        
        #boxlist mes
        
        choices = ['Janeiro', 'Fevereiro', 'Março', 'Abril','Maio',
                   'Junho','Julho','Agosto','Setembro','Outurbro','Novembro','Dezembro']
       # variable = StringVar(root)
       # variable.set('GB')

        self.MesListBox = ttk.Combobox(self.AbaTresLinhaQuatro, values = choices,state="readonly")
        self.MesListBox.set(choices[0])
                   
        self.MesListBox.config(state='disabled')
        self.MesListBox.pack(side=LEFT, anchor =NW)
        

        self.AnoLabelAnalise = Label(self.AbaTresLinhaQuatro,text="Ano:",width =7)
        self.AnoLabelAnalise.pack(side=LEFT, anchor =NW,padx=1)
        
        #boxlist ano
        
        choicesAnos = banco.selectAnos()
        
       # variable = StringVar(root)
       # variable.set('GB')

        self.AnosListBox = ttk.Combobox(self.AbaTresLinhaQuatro, values = choicesAnos,state="readonly")
        self.AnosListBox .set(choicesAnos[len(choicesAnos)-1])
        self.AnosListBox.config(state='disabled')
        self.AnosListBox.pack(side=LEFT, anchor =NW)
        

        
        self.AbaTresLinhaCinco = Frame(self.Aba3)
        self.AbaTresLinhaCinco["pady"] = 10
        self.AbaTresLinhaCinco.pack()

        self.bntGerarAnalise = Button(self.AbaTresLinhaCinco, command = lambda: self.analiseDeDados(banco))
        self.bntGerarAnalise["text"] = "Gerar Dados"
        self.bntGerarAnalise["font"] = ("Calibri", "8")
        self.bntGerarAnalise["width"] = 12
        self.bntGerarAnalise.pack( )
        
        
        
        
        self.abas.add(self.Aba2,text="Buscar/Editar")
        self.abas.add(self.Aba1,text="Inserir")
        self.abas.add(self.Aba3,text="Analise")

        self.abas.pack()
        
    def __init__(self,banco):
        self.utilite = Utilite()
        self.analitico = Analitic()
        self.master= Tk()
        self.master.geometry("700x500")
        self.master.title("Cuidado Com Dinheiro")
        self.criacaoDaTela(banco)
        return self.master.mainloop()

        
        
        