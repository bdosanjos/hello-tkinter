# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import pathlib
import sys

class Analitic:
    def __init__(self):
        return 
    
    
    
    def analiseEntreDatas(self,banco,dataInicio,dataFim):
        listaDeDados = banco.Select("",dataInicio,dataFim)
        labels =[]
        for i in range(len(listaDeDados)):
            labels.append(listaDeDados[i][0])
        labels = list(set(labels))
        
        valor=np.zeros((len(labels),), dtype=int)

        for i in range(len(listaDeDados)):
            for j in range(len(labels)):
                if(listaDeDados[i][0]== labels[j]):
                    valor[j]= valor[j] +listaDeDados[i][1]
                    

        explode= np.zeros((len(labels),), dtype=int)
        

        plt.pie(valor, explode=explode,autopct='%1.2f%%',shadow=True, radius=3)
        plt.legend(labels, loc="center")
        #plt.show()
        path =pathlib.Path(__file__).parent.absolute()
        path = str(path)
        
        sys.path.append(path)
        savefigura = path+'/graficPie.png'
        plt.savefig(savefigura,bbox_inches='tight')
        
            
        
        y_pos = np.arange(len(labels))
        fig, ax = plt.subplots()
        ax.barh(y_pos, valor, align='center')
        ax.set_yticks(y_pos)
        ax.set_yticklabels(labels)
        savefiguraBar = path+'/graficBar.png'
        plt.savefig(savefiguraBar,bbox_inches='tight')
    
        #retornar imagem para o relatorio
        #o valor total gasto por cada tupo
        #retornar o valor total gasto

        return savefigura,savefiguraBar,valor.sum()
    
        

    