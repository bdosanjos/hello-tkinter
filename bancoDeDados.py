# -*- coding: utf-8 -*-

import sqlite3
from sqlite3 import Error


class bancoDeDados:
    
    def __init__(self,path):

    
        self.connection = None
        try:
            self.connection = sqlite3.connect(path)
            print("Connection to SQLite DB successful")
        except Error as e:
            print(f"The error '{e}' occurred")
        
        

    def execute_query(self, query):
        cursor = self.connection.cursor()
        try:
            cursor.execute(query)
            self.connection.commit()
            print("Query executed successfully")
        except Error as e:
            print(f"The error '{e}' occurred")
            
            
    def execute_read_query(self, query):
        cursor = self.connection.cursor()
        result = None
        try:
            cursor.execute(query)
            result = cursor.fetchall()
            return result
        except Error as e:
            print(f"The error '{e}' occurred")
    
    
    def createDataBase(self):
        
        create_main_table = """
        CREATE TABLE IF NOT EXISTS main (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          tipo TEXT NOT NULL,
          valor FLOAT NOT NULL,
          data TEXT NOT NULL,
          descricao TEXT
        );
        """
        
        self.execute_query(create_main_table)
        
    def Select(self,tipo,data,dataFim):
        select_conta=""
        if tipo !="" and data !=dataFim:
            select_conta = """SELECT tipo, valor, data, descricao 
                            FROM main  
                            WHERE tipo ='"""+tipo+"""' AND data BETWEEN '"""+data+"""' AND '"""+dataFim+"""'
                            """


        elif tipo !="" :
    
            select_conta = """SELECT tipo, valor, data, descricao 
                            FROM main  
                            WHERE tipo ='"""+tipo+"""' AND data BETWEEN '"""+data+"""' AND '"""+dataFim+"""'
                            """
   
        
        
        elif  data !=dataFim:
       
            select_conta = """SELECT tipo, valor, data, descricao 
                            FROM main  
                            WHERE data BETWEEN '"""+data+"""' AND '"""+dataFim+"""'
                            """
        
        
   
        else:
     
            select_conta = """SELECT tipo, valor, data, descricao 
                            FROM main  
                            """
 
        return self.execute_read_query(select_conta)
    
    def selectTipos(self):
        selectTipos = """SELECT DISTINCT tipo
            FROM main  
            """
        return self.execute_read_query(selectTipos)
    def selectAnos(self):
        selectTipos = """SELECT DISTINCT data
            FROM main  
            """
        ListaAnosDistintos = self.execute_read_query(selectTipos)
        anosDistintos =[]
        for i in range(len(ListaAnosDistintos)):    
            aux = ListaAnosDistintos[i][0].split('-')
            anosDistintos.append(aux[0])

        return sorted(set(anosDistintos))
    
    def selectDescri(self,tipo,data,valor):
        seletc_descri_query = """SELECT descricao 
                    FROM main  
                    WHERE tipo ='"""+tipo+"""' 
                    AND data ='"""+data+ """'
                    AND valor='""" +valor+"""'
                    """
        return self.execute_read_query(seletc_descri_query)
    
    def update(self,oldTipo,oldData,oldValor,oldDescri,newTipo,newData,newValor,newDescri):
        update_query = """ UPDATE main
                    SET tipo ='"""+newTipo+"""',
                    data ='"""+newData+ """',
                    valor='""" +newValor+"""',
                    descricao ='""" +newDescri+"""'
                    
                    WHERE tipo ='"""+oldTipo+"""' AND
                    data ='"""+oldData+ """' AND
                    valor='""" +oldValor+"""' AND
                    descricao ='""" +oldDescri+"""'
                    
            """
        self.execute_query(update_query)
    
    def Delete(self,tipo,data,valor):
        query_delete = """ DELETE FROM main
                        WHERE tipo ='"""+tipo+"""' 
                        AND data ='"""+data+ """'
                        AND valor='""" +valor+"""'
                        """
        self.execute_query(query_delete)
    
    def InserirDAdo(self,tipo,data,valor,descricao):
        insert_despesa = "INSERT INTO main (tipo, valor, data, descricao) VALUES ('"+str(tipo)+"', "+str(valor)+", '"+str(data)+"', '"+str(descricao)+"');"
        self.execute_query(insert_despesa)